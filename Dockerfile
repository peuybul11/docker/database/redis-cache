FROM redis:latest

RUN apt-get update && apt-get install -y procps

CMD ["sh", "-c", "sysctl -w net.core.somaxconn=65535 && sysctl -w vm.overcommit_memory=1 && redis-server --appendonly yes --maxmemory 400mb --maxmemory-policy allkeys-lru"]