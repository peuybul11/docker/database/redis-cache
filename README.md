# Redis Cache

## Jika Error :
***ERROR: for redis  Cannot start service redis: failed to create shim task: OCI runtime create failed: runc create failed: sysctl "vm.overcommit_memory" is not in a separate kernel namespace: unknown***

***ERROR: for redis  Cannot start service redis: failed to create shim task: OCI runtime create failed: runc create failed: sysctl "vm.overcommit_memory" is not in a separate kernel namespace: unknown***

## Setel vm.overcommit_memory di Host
Tambahkan pengaturan *vm.overcommit_memory* ke file ``/etc/sysctl.conf`` agar perubahan ini permanen di seluruh sistem host Anda.

```
sudo bash -c 'echo "vm.overcommit_memory=1" >> /etc/sysctl.conf' && \
sudo sysctl -p
```